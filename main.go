package main

import (
	"bufio"
	"encoding/hex"
	"fmt"
	"log"
	"net"
	"os"
	"time"
)

var msgWelcome = `
Welcome to the SignatureLand!
Pass the challenge and get the flag.
`

var msgHelp = `
Available commands:
    sign          sign message
    verify        verify message signature
    challenge     start challenge
    quit          quit`

var msgPrompt = `
> `

const (
	stateCmd = iota
	stateStart
	stateSign
	stateVerifyMessage
	stateVerifySignature
	stateChallenge
)

type clientStorage struct {
	state int
	sss   *SSS

	verify struct {
		msg string
	}

	challenge struct {
		msg string
	}
}

type clientHandler struct {
	storages map[string]*clientStorage
}

func (h *clientHandler) OnTimeout(addr net.Addr, w *bufio.Writer) {
	w.WriteString("\nSorry, timeout exceed")
	w.Flush()
}

func (h *clientHandler) OnConnect(addr net.Addr, w *bufio.Writer) error {
	log.Printf("[%s] [CONNECT]", addr)

	if _, ok := h.storages[addr.String()]; !ok {
		sss, err := newSSS()
		if err != nil {
			return err
		}

		h.storages[addr.String()] = &clientStorage{
			state: stateCmd,
			sss:   sss,
		}
	}

	w.WriteString(msgWelcome + msgHelp + msgPrompt)
	w.Flush()

	return nil
}

func (h *clientHandler) OnClose(addr net.Addr) error {
	log.Printf("[%s] [CLOSE]", addr)

	if _, ok := h.storages[addr.String()]; ok {
		delete(h.storages, addr.String())
	}

	return nil
}

func (h *clientHandler) OnMessage(addr net.Addr, msg string, w *bufio.Writer) (bool, error) {
	log.Printf("[%s] [MESSAGE] %s", addr, msg)

	storage, ok := h.storages[addr.String()]

	if !ok {
		return true, fmt.Errorf("[%s] fail to get user's storage", addr)
	}

	switch storage.state {

	case stateCmd:
		switch msg {

		case "sign":
			w.WriteString("Enter message to sign: ")
			w.Flush()
			storage.state = stateSign

		case "verify":
			w.WriteString("Enter message to verify: ")
			w.Flush()
			storage.state = stateVerifyMessage

		case "challenge":
			storage.challenge.msg = randStringRunes(50)
			w.WriteString(
				fmt.Sprintf("Give me a signature for: %s\nAnswer: ", storage.challenge.msg),
			)
			w.Flush()
			storage.state = stateChallenge

		case "quit":
			w.WriteString("Goodbye!\n")
			w.Flush()
			return true, nil

		default:
			w.WriteString("Invalid command!")
			w.WriteString(msgPrompt)
			w.Flush()
			storage.state = stateCmd
		}

	case stateSign:
		s := storage.sss.Sign([]byte(msg))
		w.WriteString(fmt.Sprintf("Result: %s", hex.EncodeToString(s)))
		w.WriteString(msgPrompt)
		w.Flush()
		storage.state = stateCmd

	case stateVerifyMessage:
		storage.verify.msg = msg
		w.WriteString("Enter signature: ")
		w.Flush()
		storage.state = stateVerifySignature

	case stateVerifySignature:
		s, err := hex.DecodeString(msg)
		if err != nil {
			w.WriteString("Failure!")
			w.WriteString(msgPrompt)
			w.Flush()
			storage.state = stateCmd
			break
		}

		ok := storage.sss.Verify([]byte(storage.verify.msg), s)
		if ok {
			w.WriteString("Success!")
		} else {
			w.WriteString("Failure!")
		}

		w.WriteString(msgPrompt)
		w.Flush()
		storage.state = stateCmd

	case stateChallenge:
		s, err := hex.DecodeString(msg)
		if err != nil {
			w.WriteString("Sorry, you lose. Try again.")
			w.WriteString(msgPrompt)
			w.Flush()
			storage.state = stateCmd
			break
		}

		ok := storage.sss.Verify([]byte(storage.challenge.msg), s)
		if ok {
			w.WriteString(fmt.Sprintf("Congratulations! Your flag is %s", os.Getenv("FLAG")))
			w.Flush()
			return true, nil
		} else {
			w.WriteString("Sorry, you lose. Try again.")
			w.WriteString(msgPrompt)
			w.Flush()
			storage.state = stateCmd
		}
	}

	return false, nil
}

func main() {
	srv := &Server{
		Addr: ":1337",
		Handler: &clientHandler{
			storages: make(map[string]*clientStorage),
		},
		IdleTimeout:    time.Second * 10,
		SessionTimeout: time.Second * 60,
	}

	if err := srv.Listen(); err != nil {
		log.Fatal(err)
	}
}
