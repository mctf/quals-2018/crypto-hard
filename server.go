package main

import (
	"bufio"
	"errors"
	"io"
	"log"
	"net"
	"sync"
	"time"
)

type Handler interface {
	OnConnect(addr net.Addr, w *bufio.Writer) error
	OnMessage(addr net.Addr, msg string, w *bufio.Writer) (bool, error)
	OnClose(addr net.Addr) error
	OnTimeout(addr net.Addr, w *bufio.Writer)
}

type Conn struct {
	net.Conn
	idleTimeout   time.Duration
	maxReadBuffer int64
}

func (c *Conn) Write(p []byte) (int, error) {
	c.updateDeadline()
	return c.Conn.Write(p)
}

func (c *Conn) Read(b []byte) (int, error) {
	c.updateDeadline()
	r := io.LimitReader(c.Conn, c.maxReadBuffer)
	return r.Read(b)
}

func (c *Conn) updateDeadline() {
	idleDeadline := time.Now().Add(c.idleTimeout)
	c.Conn.SetDeadline(idleDeadline)
}

func (c *Conn) Close() error {
	return c.Conn.Close()
}

type Server struct {
	Addr           string
	Handler        Handler
	IdleTimeout    time.Duration
	SessionTimeout time.Duration

	listener   net.Listener
	conns      map[*Conn]struct{}
	mu         sync.Mutex
	inShutdown bool
}

func (s *Server) Listen() error {
	addr := s.Addr

	if addr == "" {
		addr = ":1337"
	}

	log.Printf("starting server on %v", addr)

	listener, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}

	defer listener.Close()

	s.listener = listener

	for {
		if s.inShutdown {
			break
		}

		conn, err := listener.Accept()
		if err != nil {
			log.Printf("error accepting connection %v", err)
			continue
		}

		log.Printf("accepted connection from %v", conn.RemoteAddr())

		c := &Conn{
			Conn:          conn,
			idleTimeout:   s.IdleTimeout,
			maxReadBuffer: 1 << 20,
		}

		s.addConn(c)

		c.SetDeadline(time.Now().Add(s.IdleTimeout))
		go s.handle(c)
	}

	return nil
}

func (s *Server) handle(conn *Conn) error {
	timer := time.NewTimer(s.SessionTimeout)

	addr := conn.RemoteAddr()

	if s.Handler == nil {
		return errors.New("empty handler")
	}

	defer func() {
		log.Printf("closing connection from %v", addr)

		if err := s.Handler.OnClose(addr); err != nil {
			log.Printf("error in handler's OnClose %v", err)
		}

		if err := conn.Close(); err != nil {
			log.Printf("error while closing connection %v (%v)", err, addr)
		}

		s.deleteConn(conn)
	}()

	w := bufio.NewWriter(conn)
	r := bufio.NewReader(conn)

	if err := s.Handler.OnConnect(addr, w); err != nil {
		return err
	}

	scanr := bufio.NewScanner(r)

loop:
	for {
		select {

		case <-timer.C:
			log.Printf("session timeout exceed for %v", addr)
			return nil

		default:
			scanned := scanr.Scan()
			if !scanned {
				if err := scanr.Err(); err != nil {
					log.Printf("%v (%v)", err, addr)

					if e, ok := err.(net.Error); ok && e.Timeout() {
						s.Handler.OnTimeout(addr, w)
					}

					return err
				}
				break loop
			}

			close, err := s.Handler.OnMessage(addr, scanr.Text(), w)
			if err != nil {
				return err
			}

			if close {
				return nil
			}
		}
	}

	return nil

}

func (s *Server) Shutdown() {
	s.inShutdown = true
	log.Println("shutting down...")
	s.listener.Close()

	ticker := time.NewTicker(500 * time.Millisecond)

	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			log.Printf("waiting on %v connections", len(s.conns))
		}
		if len(s.conns) == 0 {
			return
		}
	}
}

func (s *Server) addConn(c *Conn) {
	defer s.mu.Unlock()
	s.mu.Lock()

	if s.conns == nil {
		s.conns = make(map[*Conn]struct{})
	}

	s.conns[c] = struct{}{}
}

func (s *Server) deleteConn(conn *Conn) {
	defer s.mu.Unlock()
	s.mu.Lock()

	delete(s.conns, conn)
}
