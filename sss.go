package main

import (
	"crypto/rand"
	"crypto/sha512"
	"math/big"

	"golang.org/x/crypto/bn256"
)

type SSS struct {
	x    *big.Int
	X    *bn256.G2
	J, K *bn256.G1
}

func newSSS() (*SSS, error) {
	_, K, err := bn256.RandomG1(rand.Reader)
	if err != nil {
		return nil, err
	}

	_, J, err := bn256.RandomG1(rand.Reader)
	if err != nil {
		return nil, err
	}

	x, X, err := bn256.RandomG2(rand.Reader)
	if err != nil {
		return nil, err
	}

	return &SSS{x, X, J, K}, nil
}

func (sss *SSS) hash(m []byte) *bn256.G1 {
	h := sha512.Sum512(m)

	h1 := new(big.Int).SetBytes(h[:32])
	h2 := new(big.Int).SetBytes(h[32:])

	H1 := new(bn256.G1).ScalarMult(sss.K, h1)
	H2 := new(bn256.G1).ScalarMult(sss.J, h2)

	H := new(bn256.G1).Add(H1, H2)

	return H
}

func (sss *SSS) Sign(m []byte) []byte {
	H := sss.hash(m)
	S := new(bn256.G1).ScalarMult(H, sss.x)

	return S.Marshal()
}

func (sss *SSS) Verify(m, s []byte) bool {
	H := sss.hash(m)
	G2 := new(bn256.G2).ScalarBaseMult(new(big.Int).SetInt64(1))

	S, ok := new(bn256.G1).Unmarshal(s)

	if !ok {
		return false
	}

	p1 := bn256.Pair(H, sss.X)
	p2 := bn256.Pair(S, G2)

	return p1.String() == p2.String()
}
