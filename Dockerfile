FROM golang AS builder
WORKDIR /go/src/mctf-2018/crypto-hard
ADD *.go ./
RUN ls -la
RUN go get -v && CGO_ENABLED=0 go build

FROM alpine
RUN addgroup -S mctf && adduser -S -u 1001 -G mctf mctf
USER mctf
WORKDIR /home/mctf
COPY --from=builder --chown=mctf:mctf /go/src/mctf-2018/crypto-hard/crypto-hard .
EXPOSE 1337
CMD ["./crypto-hard"]
