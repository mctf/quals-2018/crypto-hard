package main

import (
	"bufio"
	"crypto/sha512"
	"encoding/hex"
	"io/ioutil"
	"log"
	"math/big"
	"net"
	"os"
	"strings"
	"testing"
	"time"

	"golang.org/x/crypto/bn256"
)

func readUntil(r *bufio.Reader, s string) (string, error) {
	read := ""

	for {
		b, err := r.ReadByte()
		if err != nil {
			return "", err
		}
		read += string(b)

		if strings.Contains(read, s) {
			break
		}
	}

	return read, nil
}

func TestSolution(t *testing.T) {
	expectedFlag := "mctf{flag}"
	os.Setenv("FLAG", expectedFlag)

	srv := &Server{
		Addr: ":1337",
		Handler: &clientHandler{
			storages: make(map[string]*clientStorage),
		},
		IdleTimeout:    time.Second * 10,
		SessionTimeout: time.Second * 60,
	}

	log.SetOutput(ioutil.Discard)

	go srv.Listen()

	// Wait until server start
	time.Sleep(time.Second * 1)

	conn, err := net.Dial("tcp", ":1337")
	if err != nil {
		t.Fatal(err)
	}

	r := bufio.NewReader(conn)
	w := bufio.NewWriter(conn)

	if _, err := readUntil(r, "> "); err != nil {
		t.Fatal(err)
	}

	// Sign message 1
	w.WriteString("sign\n")
	w.Flush()

	if _, err := readUntil(r, "to sign: "); err != nil {
		t.Fatal(err)
	}

	m1 := "msg1"
	w.WriteString(m1 + "\n")
	w.Flush()

	if _, err := readUntil(r, "Result: "); err != nil {
		t.Fatal(err)
	}

	s1, err := r.ReadString('\n')
	s1 = strings.Trim(s1, "\n")

	bs1, err := hex.DecodeString(s1)
	if err != nil {
		t.Fatal(err)
	}

	S1, ok := new(bn256.G1).Unmarshal(bs1)
	if !ok {
		t.Fatal("Can't unmarshal")
	}

	// Sign message 2
	w.WriteString("sign\n")
	w.Flush()

	if _, err := readUntil(r, "to sign: "); err != nil {
		t.Fatal(err)
	}

	m2 := "msg2"
	w.WriteString(m2 + "\n")
	w.Flush()

	if _, err := readUntil(r, "Result: "); err != nil {
		t.Fatal(err)
	}

	s2, err := r.ReadString('\n')
	s2 = strings.Trim(s2, "\n")

	bs2, err := hex.DecodeString(s2)
	if err != nil {
		t.Fatal(err)
	}

	S2, ok := new(bn256.G1).Unmarshal(bs2)
	if !ok {
		t.Fatal("Can't unmarshal")
	}

	// Calc hashes
	h_1 := sha512.Sum512([]byte(m1))
	h1 := new(big.Int).SetBytes(h_1[:32])
	h2 := new(big.Int).SetBytes(h_1[32:])

	h_2 := sha512.Sum512([]byte(m2))
	h3 := new(big.Int).SetBytes(h_2[:32])
	h4 := new(big.Int).SetBytes(h_2[32:])

	// Get J^x
	S1_h3 := new(bn256.G1).ScalarMult(S1, h3)
	S2_h1 := new(bn256.G1).ScalarMult(S2, h1)

	S2_h1_neg := new(bn256.G1).Neg(S2_h1)

	R1 := new(bn256.G1).Add(S1_h3, S2_h1_neg)

	h2_h3 := new(big.Int).Mul(h2, h3)
	h1_h4 := new(big.Int).Mul(h1, h4)

	k1 := new(big.Int).Sub(h2_h3, h1_h4)
	k1 = k1.Mod(k1, bn256.Order)
	k1 = k1.ModInverse(k1, bn256.Order)

	R1k1 := new(bn256.G1).ScalarMult(R1, k1)

	// Get K^x
	S1_h4 := new(bn256.G1).ScalarMult(S1, h4)
	S2_h2 := new(bn256.G1).ScalarMult(S2, h2)

	S2_h2_neg := new(bn256.G1).Neg(S2_h2)

	R2 := new(bn256.G1).Add(S1_h4, S2_h2_neg)

	k2 := new(big.Int).Sub(h1_h4, h2_h3)
	k2 = k2.Mod(k2, bn256.Order)
	k2 = k2.ModInverse(k2, bn256.Order)

	R2k2 := new(bn256.G1).ScalarMult(R2, k2)

	// Get flag
	w.WriteString("challenge\n")
	w.Flush()

	if _, err := readUntil(r, "for: "); err != nil {
		t.Fatal(err)
	}

	m, err := r.ReadString('\n')
	m = strings.Trim(m, "\n")

	h := sha512.Sum512([]byte(m))
	hm1 := new(big.Int).SetBytes(h[:32])
	hm2 := new(big.Int).SetBytes(h[32:])
	SS1 := new(bn256.G1).ScalarMult(R2k2, hm1)
	SS2 := new(bn256.G1).ScalarMult(R1k1, hm2)
	SS := new(bn256.G1).Add(SS1, SS2)

	if _, err := readUntil(r, "Answer: "); err != nil {
		t.Fatal(err)
	}

	w.WriteString(hex.EncodeToString(SS.Marshal()) + "\n")
	w.Flush()

	readUntil(r, "Your flag is ")

	actualFlag, _ := r.ReadString('\n')
	actualFlag = strings.Trim(actualFlag, "\n")

	if actualFlag != expectedFlag {
		t.Fatal("Flag mismatch")
	}

	conn.Close()
	srv.Shutdown()
}
