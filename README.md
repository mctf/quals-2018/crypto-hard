# Description

Welcome to the SignatureLand!
Pass the challenge and get the flag.

`nc <DOMAIN> <PORT>`

This may help you: `<SOURCE>`

# Writeup

<details>
<summary>Click to expand</summary>

You can connect to the server and sign any messages.
After you ready you can start challenge where server asks you to sign
some random message. If you provide valid signature you get flag.

Signature algorithm using on server is based on bilinear pairings (bn256).
Here is its description.

**Given**

G1, G2, GT and e(x\*G1, y\*G2) = xy\*GT (where Gx is a generator of the respective group)

**Key generation**

J, K ∈ G1

X = x\*G2 ∈ G2

J, K, X - public key

x - private key

**Sign**

m - message

h(m) = sha256(m) = h1 || h2

H = h1\*J + h2\*K - message hash

S = x\*H - signature

**Verify**

Check:

e(H, X) == e(S, G2)

**Attack**

h(m1) = h1 || h2

h(m2) = h3 || h4

Get two signatures:

S1 = x \* H1 = x \* (h1\*J + h2\*K)

S2 = x \* H2 = x \* (h3\*J + h4\*K)

S1\*h3 - S2\*h1 = h2\*h3\*x\*K - h1\*h4\*x\*K

x\*K = (S1\*h3 - S2\*h1) / (h2\*h3 - h1\*h4)

Similarly

x\*J = (S1\*h4 - S2\*h2) / (h1\*h4 - h2\*h3)

Start the challenge and get a message to sign (m)

h(m) = a || b

S = a\*x\*K + b\*x\*J = x \* (a\*K + b\*J)

[Solution](https://gitlab.com/mctf/quals-2018/crypto-hard/blob/master/main_test.go)

</details>

# Build

```sh
docker build -t mctf-crypto-hard .
```

# Run

```sh
docker run -d --rm --name mctf-crypto-hard -p 1337:1337 mctf-crypto-hard
```

# Deploy

Copy `docker-compose.yml` to the server and run:

```sh
docker-compose up -d
```
